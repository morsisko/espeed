# ESPeed
Serwis pozwalający na obsługiwanie urządzenia służącego do pomiaru przyśpieszenia pojazdu za pomocą BLE (Bluetooth Low Energy)

# Wybrane widoki

1. Ekran logowania

![](screens/desktop/login.png)

![](screens/mobile/login.png)

2. Ekran rejestracji

![](screens/desktop/register.png)

![](screens/mobile/register.png)

3. Strona główna

![](screens/desktop/index.png)

![](screens/mobile/index.png)

4. Strona podglądu ścieżki

![](screens/desktop/route.png)

![](screens/mobile/route.png)


# Kryteria ewaluacji projektu
1. Dokumentacja w pliku README.md

2. Kod napisany obiektowo

- Podział na klasy "Interface" do bazy danych i "View" do renderowania HTML

3. Diagram ERD

- Znajduje się w głównym repozytoriumm. Plik erd.pdf

4. GIT

- Regularne commity, wersja ostateczna zmergowana do gałęzi master

5. Realizacja tematu

- Zrealizowano widoki przesłane w zgłoszeniu + dwa dodatkowe (send i profile)

6. HTML

- Powtarzające się fragmenty (navbar) includowane i wydzielone do oddzielnego pliku nav.html

7. POSTGRESQL

- Połączenie z bazą PostgreSQL, połączenia przez PDO

8. Złożonośc bazy danych

- Wszystkie typy relacji:
- Jeden do wielu (User <-> Session)
- Wiele do wielu (User -> Role przez AssignedRole)
- Jeden do jednego (User -> Leaderboard)
- Zastosowane odpowiednie typy danych

9. PHP

- Zamiast PHP został użyty Python

10. JavaScript

- Użycie FETCH Api (profile.js, device.js)
- Użycie JS do walidacji formularzy (send.js)
- W każdym pliku .js przykład manipulowania elementami DOM

11. Fetch API (AJAX)

- Fetch POST -> profile.js
- Fetch GET -> device.js

12. Design

- Klasy CSS
- Brak mieszania styli w HTML i CSS


13. Responsywność

- Użycie media query

14. Logowanie

- Weryfikacja, hashowanie hasła z użyciem bcrypt

15. Sesja użytkownika

- Sesja w oparciu o baze danych i ciasteczka

16. Uprawnienia użytkowników

- Weryfikowane przez system 

17. Role użytkownika

- Rola "Admin" i "User"

18. Wylogowywanie

- Usunięcie ciasteczka, odnotowanie faktu wylogowania w bazie danych

19. Widoki, wyzwalacze lub/i funkcje

- Zastosowanie widoków:
	- leaderboard_view
	- user_router_view
	
	
- Funkcji
	- generate_leaderboard
	
20. Akcje na referencjach

- Zastosowanie JOIN np w leaderboard_view

21. Bezpieczeństwo

- Dostęp tylko dla zalogowanych, hasła hashowane bcrypt

22. Brak replikacji kodu

23. Czystość i przejrzystość kodu

24. Baza danych zrzucona do pliku db.sql

