from espeed.views.base_view import BaseView
from espeed.dbinterfaces.firmware_interface import FirmwareInterface
from flask import Response, request, redirect

class GetCurrentFirmwareView(BaseView):
    def __init__(self):
        pass

    def render(self, router, db):
        if not self.get_user_via_session_cookie(db):
            return redirect("/login")

        if request.method == "GET":
            interface = FirmwareInterface(db)
            return str(interface.get_current_version())
            #return Response(content.read())
            
        return "err"