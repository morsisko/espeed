from espeed.views.base_view import BaseView
from espeed.dbinterfaces.user import User
from espeed.dbinterfaces.user_interface import UserInterface
from flask import Response, request, redirect
import bcrypt
import string

class RegisterView(BaseView):
    def __init__(self):
        pass

    def return_error_text(self, text):
        content = open("espeed/front/register.html").read()
        content = content.replace("{%ERROR_TEXT%}", text)
        return Response(content)

    def register_user(self, router, db):
        if "email" not in request.form:
            return self.return_error_text("no email")
        
        if "password" not in request.form:
            return self.return_error_text("no password")

        if "username" not in request.form:
            return self.return_error_text("no username")

        if "repeat-password" not in request.form:
            return self.return_error_text("no repeat-password")

        username = request.form["username"]
        password = request.form["password"]
        email = request.form["email"]
        repeat_password = request.form["repeat-password"]

        if len(password) < 8:
            return self.return_error_text("you need longer password")

        if len(username) < 5:
            return self.return_error_text("you need longer username")

        for letter in username:
            if letter not in string.ascii_letters + string.digits:
                return self.return_error_text("username must consists only of latin characters")

        if password != repeat_password:
            return self.return_error_text("passwords don't match")

        user_interface = UserInterface(db)
        
        if user_interface.check_if_user_exists(email, username):
            return self.return_error_text("user with this username or password already exists")

        hashed_pass = bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt())

        user = User(email, username, hashed_pass.decode("ascii"))

        user_interface.insert(user)

        return "OK"
        
    def render(self, router, db):
        if self.get_user_via_session_cookie(db):
            return redirect("/")
            
        if request.method == "GET":
            content = open("espeed/front/register.html").read()
            content = content.replace("{%ERROR_TEXT%}", "")
            return Response(content)
            
        elif request.method == "POST":
            return self.register_user(router, db)
            
        return "err"