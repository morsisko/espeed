from espeed.views.base_view import BaseView
from flask import Response, request, redirect

class DeviceView(BaseView):
    def __init__(self):
        pass

    def render(self, router, db):
        user = self.get_user_via_session_cookie(db)
        if not user:
            return redirect("/login")

        if request.method == "GET":
            content = open("espeed/front/device.html").read()
            content = self.render_navbar(user, content)
            return Response(content)
            
        return "err"