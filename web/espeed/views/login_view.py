from espeed.views.base_view import BaseView
from espeed.dbinterfaces.user_interface import UserInterface
from espeed.dbinterfaces.session_interface import SessionInterface
from flask import Response, request, redirect, make_response
import bcrypt

class LoginView(BaseView):
    def __init__(self):
        pass

    def return_error_text(self, text):
        content = open("espeed/front/login.html").read()
        content = content.replace("{%ERROR_TEXT%}", text)
        return Response(content)

    def login_user(self, router, db):
        if "username" not in request.form:
            return self.return_error_text("no username")

        if "password" not in request.form:
            return self.return_error_text("no password")

        username = request.form["username"]
        password = request.form["password"]

        user_interface = UserInterface(db)
        user = user_interface.get_user_via_username(username)

        if not user:
            return self.return_error_text("wrong username or password")

        if not bcrypt.checkpw(password.encode("utf-8"), user.password.encode("utf-8")):
            return self.return_error_text("wrong username or password")

        session_interface = SessionInterface(db)
        session_cookie = session_interface.create_session_for_user(user)

        res = make_response(redirect("/"))
        res.set_cookie("session", value=session_cookie, max_age= 24*60*60 - 1)
        return res
         
    def render(self, router, db):
        if self.get_user_via_session_cookie(db):
            return redirect("/")

        if request.method == "GET":
            content = open("espeed/front/login.html").read()
            content = content.replace("{%ERROR_TEXT%}", "")
            return Response(content)
            
        elif request.method == "POST":
            return self.login_user(router, db)
            
        return "err"