from espeed.views.base_view import BaseView
from espeed.dbinterfaces.session_interface import SessionInterface
from flask import Response, request, make_response, redirect

class LogoutView(BaseView):
    def __init__(self):
        pass

    def render(self, router, db):
        res = make_response(redirect("/login"))
        res.set_cookie("session", value="", max_age=1)

        user = self.get_user_via_session_cookie(db)
        if not user:
            return res

        session_interface = SessionInterface(db)
        session_interface.logout_by_user_id(user.id)
        return res