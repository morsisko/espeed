from re import split
from espeed.views.base_view import BaseView
from espeed.dbinterfaces.route_interface import RouteInterface
from flask import Response, request, redirect

class RouteView(BaseView):
    def __init__(self):
        pass

    def process(self, db, user):
        splited = request.path.split("/")
        if len(splited) != 3 or not splited[2].isdecimal():
            return "no id"

        routeId = int(splited[2])
        routeInterface = RouteInterface(db)
        route = routeInterface.get_route_by_id(routeId)
        
        content = open("espeed/front/record.html").read()
        content = self.render_navbar(user, content)
        content = content.replace("{%BRAND%}", route.brand_name)
        content = content.replace("{%TIME%}", str((route.time_end - route.time_start) / 1000))
        content = content.replace("{%ENGINE%}", route.engine)
        content = content.replace("{%MODEL%}", route.model_name)
        content = content.replace("{%TRANSMISSION%}", route.transmission)
        content = content.replace("{%SCORE%}", str(route.points))
        content = content.replace("{%POS_START_LAT%}", str(route.position_start_latitude))
        content = content.replace("{%POS_START_LONG%}", str(route.position_start_longitude))
        content = content.replace("{%POS_END_LAT%}", str(route.position_end_latitude))
        content = content.replace("{%POS_END_LONG%}", str(route.position_end_longitude))
        return Response(content)

    def render(self, router, db):
        user = self.get_user_via_session_cookie(db)
        if not user:
            return redirect("/login")

        if request.method == "GET":
            return self.process(db, user)
            
        return "wrong method"