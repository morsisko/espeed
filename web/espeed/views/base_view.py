from espeed.dbinterfaces.user_interface import UserInterface
from flask import request

class BaseView:
    def __init__(self):
        pass
        
    def render(self, router, db):
        pass

    def render_navbar(self, user, content):
        nav = open("espeed/front/nav.html").read()
        content = content.replace("{%NAVBAR%}", nav)
        content = content.replace("{%USERID%}", str(user.id))
        return content

    def get_user_via_session_cookie(self, db):
        user_inteface = UserInterface(db)
        cookie = request.cookies.get('session')

        if not cookie:
            return None

        return user_inteface.get_user_via_cookie(cookie)