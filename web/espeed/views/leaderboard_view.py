from espeed.views.base_view import BaseView
from espeed.dbinterfaces.leaderboard_interface import LeaderboardInterface
from flask import Response, request, redirect

class LeaderboardView(BaseView):
    def __init__(self):
        pass

    def _generate(self, db, user):
        interface = LeaderboardInterface(db)
        content = open("espeed/front/scoreboard.html").read()

        ordinal, score = interface.get_user_place_and_score_by_nickname(user.username)

        div = ""

        for item in interface.get_leaderboard():
            div += """
                <div class="score-row">
                <div class="score-place">{}</div>
                <div class="score-avatar">{}</div>
                <div class="score-name">{}</div>
                <div class="score-score">{}</div>
                </div>
            """.format(item.ordinal, "X", item.nickname, item.score)

        content = self.render_navbar(user, content)
        content = content.replace("{%LEADERBOARD%}", div)
        content = content.replace("{%USERNAME%}", user.username)
        content = content.replace("{%SCORE%}", score)
        content = content.replace("{%PLACE%}", ordinal)
        return Response(content)

    def render(self, router, db):
        user = self.get_user_via_session_cookie(db)
        if not user:
            return redirect("/login")

        if request.method == "GET":
            return self._generate(db, user)
            
        return "err"