from re import split
from espeed.views.base_view import BaseView
from espeed.dbinterfaces.user_interface import UserInterface
from espeed.dbinterfaces.leaderboard_interface import LeaderboardInterface
from espeed.dbinterfaces.route_interface import RouteInterface
from flask import Response, request, redirect

class ProfileView(BaseView):
    def __init__(self):
        pass

    def process(self, user, db):
        splited = request.path.split("/")
        if len(splited) != 3 or not splited[2].isdecimal():
            return "no id"

        userId = int(splited[2])

        userInterface = UserInterface(db)
        leaderboardInterface = LeaderboardInterface(db)
        routeInterface = RouteInterface(db)

        remoteUser = userInterface.get_user_via_id(userId)

        if not remoteUser:
            return "404"

        place, score = leaderboardInterface.get_user_place_and_score_by_nickname(remoteUser.username)
        routes = userInterface.get_best_user_routes_by_id(userId)
        route_count = routeInterface.get_number_of_routes_by_user_id(userId)

        div = ""
        logout = ""
        trash_str = ""
        style_str = ""
        if user.id == remoteUser.id or userInterface.check_if_can_delete_routes(user.id):
            logout = '<a href="/logout">Logout</a>'
            trash_str = '<div class="score-icon"><i class="fas fa-trash"></i></div>'
            
        else:
            style_str = 'style="width: 20%;"'

        for item in userInterface.get_best_user_routes_by_id(userId):
            div += """
            <a href="/route/{}" class="score-row" data-route-id="{}">
                <div class="score-place">{}</div>
                <div class="score-name">{}</div>
                <div class="score-score" {}>{}s</div>
                {}
            </a>
            """.format(item.id, item.id, item.ordinal + 1, item.carname, style_str, item.time, trash_str)

        content = open("espeed/front/profile.html").read()
        content = self.render_navbar(user, content)
        content = content.replace("{%USERNAME%}", remoteUser.username)
        content = content.replace("{%PLACE%}", place)
        content = content.replace("{%SCORE%}", score)
        content = content.replace("{%CREATED_AT%}", remoteUser.created_at.strftime("%Y-%m-%d %H:%M:%S"))
        content = content.replace("{%ROUTE_COUNT%}", str(route_count))
        content = content.replace("{%LEADERBOARD%}", div)
        content = content.replace("{%LOGOUT%}", logout)

        return Response(content)

    def render(self, router, db):
        user = self.get_user_via_session_cookie(db)
        if not user:
            return redirect("/login")

        if request.method == "GET":
            return self.process(user, db)
            
        return "wrong method"