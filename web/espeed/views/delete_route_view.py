from re import split
from espeed.views.base_view import BaseView
from espeed.dbinterfaces.route_interface import RouteInterface
from espeed.dbinterfaces.user_interface import UserInterface
from flask import Response, request, redirect

class DeleteRouteView(BaseView):
    def __init__(self):
        pass

    def process(self, db, user):
        splited = request.path.split("/")
        if len(splited) != 3 or not splited[2].isdecimal():
            return "no id"

        routeId = int(splited[2])
        routeInterface = RouteInterface(db)
        userInterface = UserInterface(db)

        route_owner = routeInterface.get_route_owner(routeId)

        if not route_owner:
            return "no route with this id"

        if user.id != route_owner:
            if not userInterface.check_if_can_delete_routes(user.id):
                return "no permission to delete this route"

        routeInterface.delete_route_by_id(routeId)

        return "OK"

    def render(self, router, db):
        user = self.get_user_via_session_cookie(db)
        if not user:
            return redirect("/login")

        if request.method == "POST":
            return self.process(db, user)
            
        return "wrong method"