from espeed.views.base_view import BaseView
from espeed.dbinterfaces.user import User
from espeed.dbinterfaces.route_interface import RouteInterface
from espeed.dbinterfaces.car_interface import CarInterface
from flask import Response, request, redirect
import bcrypt
import string

class CommitView(BaseView):
    def __init__(self):
        pass

    def _calculatePoints(self, time):
        if time > 20:
            return 0

        if time < 1:
            return 1000

        a = -1000/19
        b = 20000/19
        return round(a * time + b)

    def _process(self, router, db, user):
        if "time-end" not in request.form:
            return "no time-end"

        if "time-start" not in request.form:
            return "no time-start"

        if "position-stop-longitude" not in request.form:
            return "no position-stop-longitude"

        if "position-stop-latitude" not in request.form:
            return "no position-stop-latitude"

        if "position-start-longitude" not in request.form:
            return "no position-start-longitude"

        if "position-start-latitude" not in request.form:
            return "no position-start-latitude"

        if "engine-input" not in request.form:
            return "no engine-input"

        if "brand-select" not in request.form:
            return "no brand-select"

        if "model-select" not in request.form:
            return "no model-select"

        if "transmission-input" not in request.form:
            return "no transmission-input"

        time_end = request.form["time-end"]
        time_start = request.form["time-start"]
        position_stop_longitude = request.form["position-stop-longitude"]
        position_stop_latitude = request.form["position-stop-latitude"]
        position_start_longitude = request.form["position-start-longitude"]
        position_start_latitude = request.form["position-start-latitude"]
        engine_input = request.form["engine-input"]
        transmission_input = request.form["transmission-input"]
        brand_select = request.form["brand-select"]
        model_select = request.form["model-select"]

        route_interface = RouteInterface(db)
        car_interface = CarInterface(db)

        car_id = car_interface.get_car_id(brand_select, model_select)

        if not car_id:
            return "no car with this parameters"

        route_interface.add_route(user.id, car_id, int(time_start), int(time_end), 
        position_start_latitude, position_start_longitude, position_stop_latitude, position_stop_longitude,
        engine_input, transmission_input, self._calculatePoints((int(time_end) - int(time_start)) / 1000))

        return redirect("/")
        
    def render(self, router, db):
        user = self.get_user_via_session_cookie(db)
        if not user:
            return redirect("/login")
            
        elif request.method == "POST":
            return self._process(router, db, user)
            
        return "err"