from espeed.views.base_view import BaseView
from espeed.dbinterfaces.car_interface import CarInterface
from flask import Response, request, redirect

class SendView(BaseView):
    def __init__(self):
        pass

    def _process(self, db, user):
        if "time-end" not in request.form:
            return "no time-end"

        if "time-start" not in request.form:
            return "no time-start"

        time_end = request.form["time-end"]
        time_start = request.form["time-start"]

        delta = (int(time_end) - int(time_start)) / 1000

        car_interface = CarInterface(db)
        brands = car_interface.get_all_car_brands()
        models = car_interface.get_all_car_models()
        brands_block = ""
        models_block = []

        for id, name in brands:
            brands_block += '<option value="{}">{}</option>'.format(id, name)

        for brand, model, name in models:
            models_block.append("[" + ",".join([str(brand), str(model), '"' + name + '"']) + "]")

        content = open("espeed/front/send.html").read()
        content = self.render_navbar(user, content)
        content = content.replace("{%TIME%}", str(delta))
        content = content.replace("{%TIME_START%}", time_start)
        content = content.replace("{%TIME_END%}", time_end)
        content = content.replace("{%BRANDS%}", brands_block)
        content = content.replace("{%MODELS%}", ",".join(models_block))
        return Response(content)

    def render(self, router, db):
        user = self.get_user_via_session_cookie(db)
        if not user:
            return redirect("/login")

        if request.method == "POST":
            return self._process(db, user)
            
        return "err"