from espeed.dbinterfaces.leaderboard_interface import LeaderboardInterface
from espeed.dbinterfaces.user_interface import UserInterface
from espeed.views.base_view import BaseView
from flask import request, redirect

class GenerateLeaderboardView(BaseView):
    def __init__(self):
        pass
        
    def render(self, router, db):
        user = self.get_user_via_session_cookie(db)
        if not user:
            return redirect("/login")

        if request.method == "GET":
            leaderboard_interface = LeaderboardInterface(db)
            user_interface = UserInterface(db)

            if user_interface.check_if_can_request_leaderboard_generation(user.id):
                leaderboard_interface.generate_leaderboard()
                return "OK"
            else:
                return "No sufficient permissions!"
            
        return "err"