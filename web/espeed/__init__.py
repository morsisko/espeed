from flask import Flask, send_from_directory
from espeed.router import Router
import psycopg2

app = Flask(__name__)
router = Router()
db = psycopg2.connect("host=db dbname=postgres user=postgres password=postgres")

@app.route('/static/<subdir>/<path:filename>/')
def static_subdir(subdir=None, filename=None):
    directory = "static/" + subdir
    return send_from_directory(directory, filename)

@app.route('/', defaults={'path': ''}, methods=['GET', 'POST'])
@app.route('/<path:path>', methods=['GET', 'POST'])
def hello(path):
    return router.route(path, db)