var brandInput = document.getElementById('brand-select');
var modelInput = document.getElementById('model-select');

function loadModels(brand) {
    empty(modelInput);
    for (var i = 0; i < modelList.length; i += 1) {
        if (modelList[i][0] != brand) {
            continue;
        }
      addOption(modelList[i][1], modelList[i][2], modelInput);
    }
}

brandInput.onchange = function (e) {
  var val = e.target.value;
    loadModels(val);
};

function empty(select) {
  select.innerHTML = '';
}

function addOption(val, name, select) {
  var option = document.createElement('option');
  option.value = val;
  option.innerHTML = name;
  select.appendChild(option);
}

loadModels(brandInput.value);