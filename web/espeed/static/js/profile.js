document.querySelectorAll(".score-icon").forEach(deleteIcon => {
    deleteIcon.addEventListener('click', function(e) {
        e.preventDefault();
         fetch("/delete_route/" + deleteIcon.parentElement.getAttribute("data-route-id"), {method: "POST"})
         .then(response => response.text()).then(txt => {
             if (txt == "OK") {
                deleteIcon.parentElement.remove();
             }
             else {
                 alert(txt);
             }
         })
    });
});