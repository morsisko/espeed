var startTime;
var stopTime;
var velocity = 0;
var timeStart = document.querySelector("#time-start");
var timeEnd = document.querySelector("#time-end");
var velocityHTML = document.querySelector(".velocity");
var startButton = document.querySelector("#start-button");
var sendButton = document.querySelector(".send-button-container");
var isStarted = false;

function changeVelocity() {
    if (!isStarted) {
        velocityHTML.innerHTML = "0";
        return;
    }

    let toIncrement = (100 / (stopTime - startTime)) * 100;
    
    velocity += toIncrement;

    velocityHTML.innerHTML = Math.round(velocity);

    if (velocity < 100) {
        setTimeout(changeVelocity, 100);
    }
    else {
        sendButton.style.visibility = "visible";
        startButton.innerHTML = "Start";
        isStarted = false;
    }
}

startButton.addEventListener('click', function() {
    sendButton.style.visibility = "hidden";
    velocity = 0;
    velocityHTML.innerHTML = "0";

    startTime = Date.now();
    stopTime = startTime + 4000 + (Math.random() * 12000);

    timeStart.value = startTime;
    timeEnd.value = Math.floor(stopTime);

    console.log(stopTime - startTime);

    if (!isStarted) {
        isStarted = true;
        startButton.innerHTML = "Stop";
        changeVelocity();
    }
    else {
        isStarted = false;
        startButton.innerHTML = "Start";
    }
});