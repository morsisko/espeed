class Route:
    def __init__(self, brand_name, model_name, time_start, time_end, \
        position_start_latitude, position_start_longitude, position_end_latitude, position_end_longitude, \
        engine, transmission, points, created_at):
        
        self.brand_name = brand_name
        self.model_name = model_name
        self.time_start = time_start
        self.time_end = time_end
        self.position_start_latitude = position_start_latitude
        self.position_start_longitude = position_start_longitude
        self.position_end_latitude = position_end_latitude
        self.position_end_longitude = position_end_longitude
        self.engine = engine
        self.transmission = transmission
        self.points = points
        self.created_at = created_at