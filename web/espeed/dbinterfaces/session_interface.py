import random
import string

class SessionInterface:
    def __init__(self, db):
        self.db = db

    def _generate_cookie(self):
        return "".join([random.choice(string.ascii_letters + string.digits) for i in range(256)])

    def create_session_for_user(self, user):  
        cookie = self._generate_cookie()

        with self.db.cursor() as c:
            c.execute("INSERT INTO public.\"Session\" (created_at, expires_at, cookie, owner) VALUES (NOW(), NOW() + INTERVAL '1 day', %s, %s)", 
            (cookie, user.id))

            self.db.commit()
            return cookie

    def logout_by_user_id(self, id):
        with self.db.cursor() as c:
            c.execute("UPDATE public.\"Session\" SET logged_out = TRUE WHERE owner = %s", (id,))

            self.db.commit()