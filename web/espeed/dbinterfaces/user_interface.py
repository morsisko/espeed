from espeed.dbinterfaces.user import User
from espeed.dbinterfaces.user_best_route import UserBestRoute

class UserInterface:
    def __init__(self, db):
        self.db = db
        
        
    def insert(self, user):
        with self.db.cursor() as c:
            c.execute('INSERT INTO public."User" (nickname, pass_salt, email, created_at) VALUES (%s, %s, %s, NOW())', 
            (user.username, user.password, user.email))

            self.db.commit()
        
    def check_if_user_exists(self, email, username):
        with self.db.cursor() as c:
            c.execute('SELECT * FROM public."User" WHERE LOWER(nickname) = LOWER(%s) OR LOWER(email) = LOWER(%s)', (username, email))
            return c.rowcount != 0

    def get_user_via_username(self, username):
        with self.db.cursor() as c:
            c.execute('SELECT id, nickname, email, pass_salt, created_at FROM public."User" WHERE nickname = %s', (username,))

            if c.rowcount != 1:
                return None

            id, nickname, email, pass_salt, created_at = c.fetchone()

            user = User(email, nickname, pass_salt, id, created_at)
            return user

    def get_user_via_id(self, id):
        with self.db.cursor() as c:
            c.execute('SELECT id, nickname, email, pass_salt, created_at FROM public."User" WHERE id = %s', (id,))

            if c.rowcount != 1:
                return None

            id, nickname, email, pass_salt, created_at = c.fetchone()

            user = User(email, nickname, pass_salt, id, created_at)
            return user

    def get_user_via_cookie(self, cookie):
        with self.db.cursor() as c:
            c.execute("""
            SELECT u.id, u.nickname, u.email, u.pass_salt, u.created_at
            FROM public."User" AS u, public."Session" AS s
            WHERE u.id = s.owner
            AND s.cookie = %s AND s.logged_out = False AND s.expires_at > NOW()""", (cookie,))

            if c.rowcount == 0:
                return None

            id, nickname, email, pass_salt, created_at = c.fetchone()

            user = User(email, nickname, pass_salt, id, created_at)
            return user

    def get_best_user_routes_by_id(self, id):
        with self.db.cursor() as c:
            c.execute('SELECT id, carname, time FROM public."user_routes_view" WHERE owner = %s;', (id,))
            if c.rowcount == 0:
                return []

            result = []
            for i, (id, carname, time) in enumerate(c.fetchall()):
                item = UserBestRoute(i, id, carname, time)
                result.append(item)

            return result

    def check_if_can_delete_routes(self, id):
        with self.db.cursor() as c:
            c.execute("""SELECT can_delete_routes from public."AssignedRole" as assignedrole, public."Role" as role, public."User" as u
            WHERE assignedrole.role = role.id AND assignedrole.user = u.id AND assignedrole.invalid_before < NOW()
            AND assignedrole.invalid_after > NOW() and u.id = %s and can_delete_routes = TRUE;""", (id,))

            return c.rowcount != 0

    def check_if_can_request_leaderboard_generation(self, id):
        with self.db.cursor() as c:
            c.execute("""SELECT can_request_leaderboard_generation from public."AssignedRole" as assignedrole, public."Role" as role, public."User" as u
            WHERE assignedrole.role = role.id AND assignedrole.user = u.id AND assignedrole.invalid_before < NOW()
            AND assignedrole.invalid_after > NOW() and u.id = %s and can_request_leaderboard_generation = TRUE;""", (id,))

            return c.rowcount != 0