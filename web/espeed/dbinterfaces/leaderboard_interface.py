from espeed.dbinterfaces.leaderboard import Leaderboard

class LeaderboardInterface:
    def __init__(self, db):
        self.db = db

    def get_leaderboard(self):
        with self.db.cursor() as c:
            c.execute('SELECT ordinal, avatar, nickname, score FROM public."leaderboard_view";')
            if c.rowcount == 0:
                return []

            result = []
            for ordinal, avatar, nickname, score in c.fetchall():
                item = Leaderboard(ordinal, avatar, nickname, score)
                result.append(item)

            return result

    def get_user_place_and_score_by_nickname(self, nickname):
        with self.db.cursor() as c:
            c.execute('SELECT ordinal, score FROM public."leaderboard_view" WHERE nickname = %s;', (nickname,))

            if c.rowcount == 0:
                return ("?", "?")

            ordinal, score = c.fetchone()

            return str(ordinal), str(score)

    def generate_leaderboard(self):
        with self.db.cursor() as c:
            c.execute('SELECT generate_leaderboard();')

            self.db.commit()