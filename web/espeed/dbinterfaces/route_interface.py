from espeed.dbinterfaces.route import Route

class RouteInterface:
    def __init__(self, db):
        self.db = db

    def get_route_by_id(self, id):
        with self.db.cursor() as c:
            c.execute("""SELECT 
            brand.name, model.name, record.time_start, record.time_end, 
            record.position_start_latitude, record.position_start_longitude, record.position_end_latitude, record.position_end_longitude,
            record.engine, record.transmission, record.points, record.created_at
            FROM
            public."Record" as record, public."Car" as car, public."CarBrand" as brand, public."CarModel" as model
            WHERE
            record.car = car.id AND car.model = model.id AND car.brand = brand.id
            AND
            record.id = %s""", (id,))

            if c.rowcount != 1:
                return None

            brand_name, model_name, time_start, time_end, \
                position_start_latitude, position_start_longitude, position_end_latitude, position_end_longitude, \
                engine, transmission, points, created_at = c.fetchone()
            
            route = Route(brand_name, model_name, time_start, time_end, \
                position_start_latitude, position_start_longitude, position_end_latitude, \
                    position_end_longitude, engine, transmission, points, created_at)

            return route

    def get_number_of_routes_by_user_id(self, id):
        with self.db.cursor() as c:
            c.execute("""SELECT
            COUNT(*) FROM public."Record" as record
            WHERE record.owner = %s""", (id,))

            if c.rowcount != 1:
                return None

            count, = c.fetchone()

            return count

    def delete_route_by_id(self, id):
        with self.db.cursor() as c:
            c.execute("""DELETE
            FROM public."Record" as record
            WHERE record.id = %s""", (id,))

            self.db.commit()
            
            if c.rowcount != 1:
                return False

            return True

    def add_route(self, user_id, car_id, time_start, time_end, position_start_latitude, position_start_longitude, position_end_latitude, position_end_longitude, engine, transmission, points):
        with self.db.cursor() as c:
            c.execute("""
            INSERT INTO public."Record" 
            (owner, position_start_latitude, position_start_longitude, position_end_latitude, position_end_longitude,
            time_start, time_end, car, engine, transmission, points, created_at) VALUES
            (%s, %s, %s, %s, %s,
            %s, %s, %s, %s, %s, %s, NOW());
            """, (user_id, position_start_latitude, position_start_longitude, position_end_latitude, position_end_longitude,
            time_start, time_end, car_id, engine, transmission, points))

            self.db.commit()

    def get_route_owner(self, route_id):
        with self.db.cursor() as c:
            c.execute("""SELECT owner
            FROM public."Record" as record
            WHERE record.id = %s""", (route_id,))

            if c.rowcount != 1:
                return None

            owner, = c.fetchone()

            return owner