class CarInterface:
    def __init__(self, db):
        self.db = db

    def get_car_id(self, brand, model):
        with self.db.cursor() as c:
            c.execute('SELECT id FROM public."Car" WHERE brand = %s AND model = %s;', (brand, model))

            if c.rowcount != 1:
                return None

            id, = c.fetchone()
            return id

    def get_all_car_brands(self):
        with self.db.cursor() as c:
            c.execute("""SELECT brand.id, brand.name FROM public."Car" as car, public."CarBrand" as brand 
            WHERE brand.id = car.brand;""")

            if c.rowcount == 0:
                return []

            result = []
            for brand, name in c.fetchall():
                result.append((brand, name))

            return result

    def get_all_car_models(self):
        with self.db.cursor() as c:
            c.execute("""SELECT car.brand, model.id, model.name FROM public."Car" as car, public."CarModel" as model 
            WHERE model.id = car.model;""")

            if c.rowcount == 0:
                return []

            result = []
            for brand, model, name in c.fetchall():
                result.append((brand, model, name))

            return result