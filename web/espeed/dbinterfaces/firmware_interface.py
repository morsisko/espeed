class FirmwareInterface:
    def __init__(self, db):
        self.db = db

    def get_current_version(self):
        with self.db.cursor() as c:
            c.execute('SELECT version FROM public."Firmware" ORDER BY version DESC LIMIT 1;')

            if c.rowcount != 1:
                return None

            version, = c.fetchone()
            return version