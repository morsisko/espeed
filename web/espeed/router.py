from werkzeug.utils import escape
from espeed.views.login_view import LoginView
from espeed.views.register_view import RegisterView
from espeed.views.index_view import IndexView
from espeed.views.logout_view import LogoutView
from espeed.views.route_view import RouteView
from espeed.views.leaderboard_view import LeaderboardView
from espeed.views.generate_leaderboard_view import GenerateLeaderboardView
from espeed.views.get_current_firmware_view import GetCurrentFirmwareView
from espeed.views.device_view import DeviceView
from espeed.views.profile_view import ProfileView
from espeed.views.delete_route_view import DeleteRouteView
from espeed.views.send_view import SendView
from espeed.views.commit_view import CommitView

class Router:
    MAPPING = {
        'login' : LoginView(),
        'register' : RegisterView(),
        'logout' : LogoutView(),
        'route' : RouteView(),
        'scoreboard' : LeaderboardView(),
        'device' : DeviceView(),
        'profile' : ProfileView(),
        'send' : SendView(),
        'commit' : CommitView(),
        'generate_leaderboard' : GenerateLeaderboardView(),
        'get_firmware' : GetCurrentFirmwareView(),
        'delete_route' : DeleteRouteView(),
        '' : IndexView()
    }
    def __init__(self):
        pass
        
    def route(self, path, db):
        splited = path.split("/")
        path = ""
        if len(splited) >= 1:
            path = splited[0]


        if path in Router.MAPPING:
            return Router.MAPPING[path].render(self, db)
            
        return "404"
