--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1 (Debian 13.1-1.pgdg100+1)
-- Dumped by pg_dump version 13.5

-- Started on 2022-01-24 18:08:24 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 228 (class 1255 OID 49152)
-- Name: generate_leaderboard(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.generate_leaderboard() RETURNS void
    LANGUAGE sql
    AS $$
DELETE FROM public."Leaderboard";
INSERT INTO public."Leaderboard" ("user", score, ordinal)
SELECT r.owner as user, SUM(r.points) as score,
RANK() OVER ( 
	ORDER BY SUM(r.points) DESC 
) AS ordinal
FROM public."Record" as r
GROUP BY r.owner;
$$;


ALTER FUNCTION public.generate_leaderboard() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 221 (class 1259 OID 32877)
-- Name: AssignedRole; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."AssignedRole" (
    id integer NOT NULL,
    invalid_before timestamp without time zone,
    invalid_after timestamp without time zone,
    created_at timestamp without time zone,
    "user" integer,
    role integer
);


ALTER TABLE public."AssignedRole" OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 32875)
-- Name: AssignedRole_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."AssignedRole_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."AssignedRole_id_seq" OWNER TO postgres;

--
-- TOC entry 3113 (class 0 OID 0)
-- Dependencies: 220
-- Name: AssignedRole_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."AssignedRole_id_seq" OWNED BY public."AssignedRole".id;


--
-- TOC entry 207 (class 1259 OID 32806)
-- Name: Car; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Car" (
    id integer NOT NULL,
    brand integer,
    model integer,
    photo_path character varying
);


ALTER TABLE public."Car" OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 32784)
-- Name: CarBrand; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."CarBrand" (
    id integer NOT NULL,
    name character varying
);


ALTER TABLE public."CarBrand" OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 32782)
-- Name: CarBrand_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."CarBrand_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."CarBrand_id_seq" OWNER TO postgres;

--
-- TOC entry 3114 (class 0 OID 0)
-- Dependencies: 202
-- Name: CarBrand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."CarBrand_id_seq" OWNED BY public."CarBrand".id;


--
-- TOC entry 205 (class 1259 OID 32795)
-- Name: CarModel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."CarModel" (
    id integer NOT NULL,
    name character varying
);


ALTER TABLE public."CarModel" OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 32793)
-- Name: CarModel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."CarModel_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."CarModel_id_seq" OWNER TO postgres;

--
-- TOC entry 3115 (class 0 OID 0)
-- Dependencies: 204
-- Name: CarModel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."CarModel_id_seq" OWNED BY public."CarModel".id;


--
-- TOC entry 206 (class 1259 OID 32804)
-- Name: Car_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Car_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Car_id_seq" OWNER TO postgres;

--
-- TOC entry 3116 (class 0 OID 0)
-- Dependencies: 206
-- Name: Car_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Car_id_seq" OWNED BY public."Car".id;


--
-- TOC entry 209 (class 1259 OID 32817)
-- Name: Device; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Device" (
    id integer NOT NULL,
    mac_address character varying,
    name character varying,
    owner integer,
    bluetooth_name character varying,
    last_seen timestamp without time zone,
    firmware integer
);


ALTER TABLE public."Device" OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 32815)
-- Name: Device_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Device_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Device_id_seq" OWNER TO postgres;

--
-- TOC entry 3117 (class 0 OID 0)
-- Dependencies: 208
-- Name: Device_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Device_id_seq" OWNED BY public."Device".id;


--
-- TOC entry 219 (class 1259 OID 32866)
-- Name: EmailConfirmation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."EmailConfirmation" (
    id integer NOT NULL,
    address character varying,
    expired_after timestamp without time zone,
    used boolean,
    requested_at timestamp without time zone,
    owner integer
);


ALTER TABLE public."EmailConfirmation" OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 32864)
-- Name: EmailConfirmation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."EmailConfirmation_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."EmailConfirmation_id_seq" OWNER TO postgres;

--
-- TOC entry 3118 (class 0 OID 0)
-- Dependencies: 218
-- Name: EmailConfirmation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."EmailConfirmation_id_seq" OWNED BY public."EmailConfirmation".id;


--
-- TOC entry 215 (class 1259 OID 32847)
-- Name: Firmware; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Firmware" (
    id integer NOT NULL,
    version character varying,
    hash character(1),
    signature character(1),
    amount_of_downloads integer
);


ALTER TABLE public."Firmware" OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 32845)
-- Name: Firmware_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Firmware_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Firmware_id_seq" OWNER TO postgres;

--
-- TOC entry 3119 (class 0 OID 0)
-- Dependencies: 214
-- Name: Firmware_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Firmware_id_seq" OWNED BY public."Firmware".id;


--
-- TOC entry 217 (class 1259 OID 32858)
-- Name: ForgottenPasswords; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."ForgottenPasswords" (
    id integer NOT NULL,
    expired_after timestamp without time zone,
    used boolean,
    requested_at timestamp without time zone,
    owner integer
);


ALTER TABLE public."ForgottenPasswords" OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 32856)
-- Name: ForgottenPasswords_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."ForgottenPasswords_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."ForgottenPasswords_id_seq" OWNER TO postgres;

--
-- TOC entry 3120 (class 0 OID 0)
-- Dependencies: 216
-- Name: ForgottenPasswords_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."ForgottenPasswords_id_seq" OWNED BY public."ForgottenPasswords".id;


--
-- TOC entry 213 (class 1259 OID 32839)
-- Name: Leaderboard; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Leaderboard" (
    id integer NOT NULL,
    ordinal integer,
    "user" integer,
    score integer
);


ALTER TABLE public."Leaderboard" OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 32837)
-- Name: Leaderboard_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Leaderboard_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Leaderboard_id_seq" OWNER TO postgres;

--
-- TOC entry 3121 (class 0 OID 0)
-- Dependencies: 212
-- Name: Leaderboard_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Leaderboard_id_seq" OWNED BY public."Leaderboard".id;


--
-- TOC entry 211 (class 1259 OID 32828)
-- Name: Record; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Record" (
    id integer NOT NULL,
    owner integer,
    position_start_latitude numeric,
    position_start_longitude numeric,
    position_end_latitude numeric,
    position_end_longitude numeric,
    time_start bigint,
    time_end bigint,
    car integer,
    engine character varying,
    transmission character varying,
    points integer,
    created_at timestamp without time zone
);


ALTER TABLE public."Record" OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 32826)
-- Name: Record_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Record_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Record_id_seq" OWNER TO postgres;

--
-- TOC entry 3122 (class 0 OID 0)
-- Dependencies: 210
-- Name: Record_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Record_id_seq" OWNED BY public."Record".id;


--
-- TOC entry 223 (class 1259 OID 32885)
-- Name: Role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Role" (
    id integer NOT NULL,
    name character varying,
    can_delete_routes boolean,
    can_upload_new_cars boolean,
    can_request_leaderboard_generation boolean,
    can_request_firmware_upgrade boolean
);


ALTER TABLE public."Role" OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 32883)
-- Name: Role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Role_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Role_id_seq" OWNER TO postgres;

--
-- TOC entry 3123 (class 0 OID 0)
-- Dependencies: 222
-- Name: Role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Role_id_seq" OWNED BY public."Role".id;


--
-- TOC entry 225 (class 1259 OID 32896)
-- Name: Session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Session" (
    id integer NOT NULL,
    created_at timestamp without time zone,
    expires_at timestamp without time zone,
    cookie character varying,
    logged_out boolean DEFAULT false,
    owner integer
);


ALTER TABLE public."Session" OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 32894)
-- Name: Session_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Session_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Session_id_seq" OWNER TO postgres;

--
-- TOC entry 3124 (class 0 OID 0)
-- Dependencies: 224
-- Name: Session_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Session_id_seq" OWNED BY public."Session".id;


--
-- TOC entry 201 (class 1259 OID 32772)
-- Name: User; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."User" (
    id integer NOT NULL,
    nickname character varying,
    pass_salt character varying,
    email character varying,
    avatar character varying DEFAULT 'avatar.png'::character varying,
    created_at timestamp without time zone
);


ALTER TABLE public."User" OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 32770)
-- Name: User_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."User_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."User_id_seq" OWNER TO postgres;

--
-- TOC entry 3125 (class 0 OID 0)
-- Dependencies: 200
-- Name: User_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."User_id_seq" OWNED BY public."User".id;


--
-- TOC entry 226 (class 1259 OID 49157)
-- Name: leaderboard_view; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.leaderboard_view AS
 SELECT l.ordinal,
    u.avatar,
    u.nickname,
    l.score
   FROM (public."Leaderboard" l
     JOIN public."User" u ON ((u.id = l."user")));


ALTER TABLE public.leaderboard_view OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 49182)
-- Name: user_routes_view; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.user_routes_view AS
 SELECT record.id,
    usr.id AS owner,
    concat(carbrand.name, ' ', carmodel.name) AS carname,
    trunc((((record.time_end - record.time_start))::numeric / (1000)::numeric), 3) AS "time"
   FROM public."CarBrand" carbrand,
    public."CarModel" carmodel,
    public."Record" record,
    public."Car" car,
    public."User" usr
  WHERE ((carbrand.id = car.brand) AND (carmodel.id = car.model) AND (record.car = car.id) AND (record.owner = usr.id))
  ORDER BY (trunc((((record.time_end - record.time_start))::numeric / (1000)::numeric), 3))
 LIMIT 50;


ALTER TABLE public.user_routes_view OWNER TO postgres;

--
-- TOC entry 2905 (class 2604 OID 32880)
-- Name: AssignedRole id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."AssignedRole" ALTER COLUMN id SET DEFAULT nextval('public."AssignedRole_id_seq"'::regclass);


--
-- TOC entry 2898 (class 2604 OID 32809)
-- Name: Car id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Car" ALTER COLUMN id SET DEFAULT nextval('public."Car_id_seq"'::regclass);


--
-- TOC entry 2896 (class 2604 OID 32787)
-- Name: CarBrand id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."CarBrand" ALTER COLUMN id SET DEFAULT nextval('public."CarBrand_id_seq"'::regclass);


--
-- TOC entry 2897 (class 2604 OID 32798)
-- Name: CarModel id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."CarModel" ALTER COLUMN id SET DEFAULT nextval('public."CarModel_id_seq"'::regclass);


--
-- TOC entry 2899 (class 2604 OID 32820)
-- Name: Device id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Device" ALTER COLUMN id SET DEFAULT nextval('public."Device_id_seq"'::regclass);


--
-- TOC entry 2904 (class 2604 OID 32869)
-- Name: EmailConfirmation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."EmailConfirmation" ALTER COLUMN id SET DEFAULT nextval('public."EmailConfirmation_id_seq"'::regclass);


--
-- TOC entry 2902 (class 2604 OID 32850)
-- Name: Firmware id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Firmware" ALTER COLUMN id SET DEFAULT nextval('public."Firmware_id_seq"'::regclass);


--
-- TOC entry 2903 (class 2604 OID 32861)
-- Name: ForgottenPasswords id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ForgottenPasswords" ALTER COLUMN id SET DEFAULT nextval('public."ForgottenPasswords_id_seq"'::regclass);


--
-- TOC entry 2901 (class 2604 OID 32842)
-- Name: Leaderboard id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Leaderboard" ALTER COLUMN id SET DEFAULT nextval('public."Leaderboard_id_seq"'::regclass);


--
-- TOC entry 2900 (class 2604 OID 32831)
-- Name: Record id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Record" ALTER COLUMN id SET DEFAULT nextval('public."Record_id_seq"'::regclass);


--
-- TOC entry 2906 (class 2604 OID 32888)
-- Name: Role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Role" ALTER COLUMN id SET DEFAULT nextval('public."Role_id_seq"'::regclass);


--
-- TOC entry 2907 (class 2604 OID 32899)
-- Name: Session id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Session" ALTER COLUMN id SET DEFAULT nextval('public."Session_id_seq"'::regclass);


--
-- TOC entry 2894 (class 2604 OID 32775)
-- Name: User id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."User" ALTER COLUMN id SET DEFAULT nextval('public."User_id_seq"'::regclass);


--
-- TOC entry 3102 (class 0 OID 32877)
-- Dependencies: 221
-- Data for Name: AssignedRole; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."AssignedRole" (id, invalid_before, invalid_after, created_at, "user", role) FROM stdin;
1	2022-01-22 19:04:53.245142	2023-01-22 19:04:53.245142	2022-01-22 19:04:53.245142	1	1
\.


--
-- TOC entry 3088 (class 0 OID 32806)
-- Dependencies: 207
-- Data for Name: Car; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Car" (id, brand, model, photo_path) FROM stdin;
1	1	1	x
2	2	2	x
3	3	3	x
\.


--
-- TOC entry 3084 (class 0 OID 32784)
-- Dependencies: 203
-- Data for Name: CarBrand; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."CarBrand" (id, name) FROM stdin;
1	Fiat
2	Skoda
3	Unknown
\.


--
-- TOC entry 3086 (class 0 OID 32795)
-- Dependencies: 205
-- Data for Name: CarModel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."CarModel" (id, name) FROM stdin;
1	Panda
2	Octavia
3	Unknown
\.


--
-- TOC entry 3090 (class 0 OID 32817)
-- Dependencies: 209
-- Data for Name: Device; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Device" (id, mac_address, name, owner, bluetooth_name, last_seen, firmware) FROM stdin;
\.


--
-- TOC entry 3100 (class 0 OID 32866)
-- Dependencies: 219
-- Data for Name: EmailConfirmation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."EmailConfirmation" (id, address, expired_after, used, requested_at, owner) FROM stdin;
\.


--
-- TOC entry 3096 (class 0 OID 32847)
-- Dependencies: 215
-- Data for Name: Firmware; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Firmware" (id, version, hash, signature, amount_of_downloads) FROM stdin;
1	1	x	d	1
\.


--
-- TOC entry 3098 (class 0 OID 32858)
-- Dependencies: 217
-- Data for Name: ForgottenPasswords; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."ForgottenPasswords" (id, expired_after, used, requested_at, owner) FROM stdin;
\.


--
-- TOC entry 3094 (class 0 OID 32839)
-- Dependencies: 213
-- Data for Name: Leaderboard; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Leaderboard" (id, ordinal, "user", score) FROM stdin;
23	1	1	4426
24	2	2	682
\.


--
-- TOC entry 3092 (class 0 OID 32828)
-- Dependencies: 211
-- Data for Name: Record; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Record" (id, owner, position_start_latitude, position_start_longitude, position_end_latitude, position_end_longitude, time_start, time_end, car, engine, transmission, points, created_at) FROM stdin;
6	1	50.071374	19.944850	50.073505	19.944391	1642861950888	1642861959273	2	TPI	Manual	1337	2022-01-22 17:14:09.400558
8	1	50.071374	19.944850	50.073505	19.944391	1642883265604	1642883280970	1	yeds	yes	1337	2022-01-22 20:27:42.518339
9	1	50.071374	19.944850	50.073505	19.944391	1642950329695	1642950336385	1	Auto	Auto	1337	2022-01-23 15:00:38.387717
10	1	50.071374	19.944850	50.073505	19.944391	1642973221552	1642973233673	2	xd	dx	415	2022-01-23 21:27:00.661254
11	2	50.071374	19.944850	50.073505	19.944391	1643045732440	1643045739482	1	Auto	Auto	682	2022-01-24 17:34:55.350782
\.


--
-- TOC entry 3104 (class 0 OID 32885)
-- Dependencies: 223
-- Data for Name: Role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Role" (id, name, can_delete_routes, can_upload_new_cars, can_request_leaderboard_generation, can_request_firmware_upgrade) FROM stdin;
1	Admin	t	t	t	t
\.


--
-- TOC entry 3106 (class 0 OID 32896)
-- Dependencies: 225
-- Data for Name: Session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Session" (id, created_at, expires_at, cookie, logged_out, owner) FROM stdin;
2	2022-01-08 14:36:11.355142	2022-01-09 14:36:11.355142	kDetyGOguAsPyCNdDY1y8nok7yQwTVXerdkEXT9mnsmNfcf5pC4fbSbP2xZKhM33ILF9iQiD5JdQZ3JYa8V3EFRYbYAhW9RJclWFF03mm0SblXHy8DvgJi4Vdy6klko6pLWTRkZ77l3ZDp3XU4kpAuW3SvbejtUTKN3LABWZx1exOE7OQrNMVyqXkw3XwPdixrYXpKW1dI0RXClA7glIXYcCv0r8LUfti5dG9g2QvG9lvW8dqyAn0eafCOOUAPKz	t	2
1	2022-01-07 00:42:22.901933	2022-01-08 00:42:22.901933	ermZQFwB7pqVnQYiVy1O2sCjQGOAwFCgb4Yfi8cDh2y0X1TkPf1uIes9vcNzf0bI0yuWyUxMzc81DMyHeZv1YE5SLZAoYoaCof9dYlIBGvhNyb0ZvZmpecxev1fqcsfk3yAvJV79P7Jnyq0Ry1yvoCHQL6XNdFgQUwuN7NMRnZMqBRoaRsSewBoYGMWvjgLDd0IaVeJJCdZVCvFzCRj8PT0ElALIkgOLvVrbQc7Hx8UEg2g9aApMHoNB4IxRLKht	t	1
3	2022-01-12 18:34:26.597919	2022-01-13 18:34:26.597919	0tzK4N5HVqh9ZYAcE7eAdv2yExjHP0HAo0UpEEEVdcAPq0Sq0XCxp9Dj2GiHOXvRk2vxi2ohNm9vaqQIIlwxuzgnZX5Tej6L2RzrMMjlfnzshIhW5Vmt5AXpKi5YrwqKUdyDKwxBlViNlH2Ys9M5VvWEWTeKtQQyLVwYV3zLbNnG1R3NOJ4cXcK4pXcMsfwoQCEKTwCe796qpvxn1nvf5YsksMAyhLypkUEjaswDghlFBw7LvnWCE68Ie6Dgdt9k	t	1
4	2022-01-15 13:38:31.089145	2022-01-16 13:38:31.089145	7biAcAYdyd2TXCImbWj2PDu3ahA0BdfsNoSbdX7d7gN7gh6alOZYWzeBYeSFr0hd5h2w7UXIPeVFwo53krnVqH99MwHUPD7ZdpRlkKlMUvqGjbl0EmlBXNhJJOTjkCVJzUvE54shuhUHTHDW7ogBCqBwfu32l53ZJfkWBdXsjE8h5jYsGba9j2yURR6Olu33hRfVGqgJ8h0koiOFMXDnqxqMQtcf46HTiFBrigbpraVSdCPnoM65WaIRcpr1uqsK	t	1
5	2022-01-15 19:26:47.582854	2022-01-16 19:26:47.582854	UeBWqt7rqSWtQSJiYHJjy7pwH2fnFLi6KmTQ67mrZxza4me2ldwlMllItX83gP8FHyPArh9GAmc50zXwrg6o5hGaT5zqPnXFs4JqSwO1RQHQsHPHsm28cphtBfVgvbEq1XJyhK1CEEHDlMaNQk1TyrXGDLB6Y2IruNE0R0dfOK2JIeg6QP9hr74dDtHsEVsBNEOuDDReK0Bx7kEjkQcCej6jYcDnNHe37r9YxhH5n4BrQ69bLoHZRBoOtXlOWJ8i	t	1
10	2022-01-22 20:03:17.345917	2022-01-23 20:03:17.345917	LazPo3hrWtZds9p3uIv5VOXHdnXqsGxFLcCh6GBrrgBToMzNwyaCyDJta8Oapc0PgjzhTXlHSUjblNcpbbvlkaU9VD3MIDdxh7gDqbniPznDTPdaLLB52gY0bZcn98O8pHCmtSMnQat6u6WpGx3Vx7XNqvkhgCLbJoXHi4LveiVpfCbRvmmIk57MJbcBiqwMFMJB94zOYa0Dj2NgrhMyuOYWO4Ly7XXIV281KDLYvQSqKFUsPdjEJRoJGt8Kl1Z9	t	2
11	2022-01-22 20:10:26.848725	2022-01-23 20:10:26.848725	Gy5JAQSEH2m8xwBwCdajXskPW9p4sQwpOp3MMSgkicRyfn3ucVZ4sOpY6CqxXQRnhfymHjDCPoszPs04HKgTAMsLGcxvfn3I9xIN6qQjbQsccgaTyUrVbZ5ZEsxvgKLsvxL9zLBV5ysykA8yDj44cjiLSzmTO4Iuf4IPSMNFljp5fcqvwZha4aWnk1uRuRPTgfbcIvWrBEiDGVfPqt5WX5p0NOq21yzWY2pYLC3tjMFwG2awZqZRMkdvE4LgpEHd	t	2
6	2022-01-15 20:57:01.382663	2022-01-16 20:57:01.382663	Fqcu9iiGWqOCRMWT7pG7SlAoqtlSFoCmkHy6IezWabExCVhJM4uFyiHDYXEqGmTCLrLuOWiqU3LU8V3C9PM6feAfdpT5q9oy0dPeIfmci7zMsKo8Klz7WRzHVNcyZpWRkxBp902bAZnrBMqnaNMfrDcDXebCSEaHxfyBwRafCxo7SRKn3OqaedwUPMKCc6qgOENIBnGTCJLy17nX2bqgRGdxaKdPIaGmr1bXy1jeN0nj9FKEnD7KcoBksa5myQk9	t	1
7	2022-01-16 18:54:26.673742	2022-01-17 18:54:26.673742	w42gIocPgepY9GqwyJp5CYZp2H84OjFfN8KzETpLJH5f3P44YXcRTkGa0rQboUTHJmCNyUXyGGwsPPGLQr0jmCiLqaRVkqRZQEuMMm8wOQmlD3ixniioToovmnTo0RFKJjUg6iyMguujgeZtOPqdvnUClDIXpHfNSk5dakfVe0iYh2ipCsH6RowUwyDRJGAehXPHUMleg44Dph6GCFyLlEjnfQhC0jKWlMPZZ8NTvy6r1zPbZ40rsrxDCDMMgYJi	t	1
8	2022-01-19 21:24:38.504779	2022-01-20 21:24:38.504779	sEQmUkIJe9VJ94dFMFexjuc1zJgATKSRZltdEccMARIAFKscrLTUaoHSEuV85lCORjmiZjBy3wZvnVigHpsR2ZikwLOIqVFJsoSpiUPB0cylgRbsAHK1SPlcE4f1KK8UpSkQ3r167KHlVBSKy48278npWgZjYML7XYJq61lXm0qvbvpUIy9sBqHde3PhqfYaHYRTXvfSmiFWOdDoLwX9J7QQMOAB460FQ4O0yWnRfp6v2PE03BcCtnxm9Dq6Trwd	t	1
9	2022-01-21 23:47:03.589525	2022-01-22 23:47:03.589525	LGOapvhtpAlV14YTlynUYOg2DcIfeQA7R78yJAww0CvSzKriliFgX9jpk4aOV2jRMJmfhIJHzaMny9NeoyEIpsLjR1wzwQnfTl6g8KaAa4b4qXgUVFkJn1Q5rLzjMm5SXouZT08khGZScVkqo16nGE9DgJTHH7HwSnA7qcZ7jIDt9QfAUzY7DazA6gI7N2mJsrNuzYLj9W4aZDsv1DgIL8cx1KGXxNNqQxQbYiodL3zOK5auCthBS49O6hBbFR23	t	1
12	2022-01-22 20:24:39.204591	2022-01-23 20:24:39.204591	c9BVslGazevgXhTliCBIIVFRhXVWLsjHHGLgusnXEbHyjHUliYpQY2Je9YRHWMX2hReC5GyyhPmKVyhqQCER9RC2mTEcEWnyYmNYFsnoIHitcfv4qu8D1DDuTcQFWUFAx9OFigoP0PY0sD7ouMdwzR6hGiO0n6IMoKrzQaEnBDORjM4Eng0Pjf8CO2GIfNi6iw3T18tPV4XQ58juGySrozTkv1djCp9ORfxOmTI4hxngexzjECDg7mWPAEP8zbT1	t	1
13	2022-01-22 20:24:53.1655	2022-01-23 20:24:53.1655	mKTfqhOjN9PoBQYrdN95s7HHHIFs0grk3YZnmUzx3F04WsbfSGsugWynvinW1DU57DriKQzN6DJ5rlb4Pl7YmGb8cNTlkwVS78lWHJUxIsrPkpVJnkTM9R4jGTBJJ6izBahmxxlYf6ZrUvtQ2mpDFZIONHUec6R6J9oABrxoJZLTrYSHzHlf4bK2kgZOb8XesGPEnKtjA4P5yHF3R31dgBv5TeLyJR0dybzuvPIXQAzu69c0daMWmEMSDW3Q8isx	t	1
14	2022-01-23 21:16:03.32802	2022-01-24 21:16:03.32802	1eHaBVgGhPSfp1Zv8LGaRD2yLglDHDVYJqj1oIMRWIcnD89PSrxzAqErlYfT7f8XUc9XIJXlqXPsVNR7dmseDbInax3GaNCrxwxJ6ipkCqxVyjijn8E9W4CMo34M8cqWAmh31kSOpTMtFa6EbEQ2BiFVgvQDVTGBDwfeKju8HjeYFP9ROyF5JldFoAbfUosrqMn8YmuHn0oDTKbW6aQkUROFP6qZsPOr4wcUFlUGp8VbbBEvSkJahNLu08cGNtKy	t	1
15	2022-01-23 21:16:04.334478	2022-01-24 21:16:04.334478	efa9pSw7yI2g5WzsR1G0XtpOUb8z7JfSTyDOPnrwMoitj2q7C7afht1juHNjYJLSuoGRUnSuOFScqM8vo20UUWNJ34TvoErxZg0UC9l67XeHkcNat93ikMR62mIQ2Ruz4k8sf5m5qFJ4hOGrQpMbhIfMl1923VEDx5grnUnU2rvceRpbRbriGu23zeRRF82wGje3pKMTtu9xU64kPMNHqU3QEJW2RQl1i2RTIND9E2MIhy5DA5fKk4TJT3oPPXuz	t	1
16	2022-01-24 17:34:54.976028	2022-01-25 17:34:54.976028	4KNwjpuPzVQdgoWRJ8cFFbeYIunJHsAFsYMcdKlyOlhMJHqGLVZXk3VqpFnolbJ4abYImQ4xDvB6jEv2ba9kvXEqWZFdEnqBL8uJmdV0AF4EEzDVpz9AHcBaqRWL5oRRtGPx3bNh0UMRo8lzBYSePc4BdSkX37WbOwz3Gp2kF7dWAuEObxbfVGy5T8TCDKZgbgQRt89ZBt7bJowHHo4LQ3P120FPiXwTWaFV2deoFHFG3b7dZqMZiK6g6eVWNIXr	f	2
17	2022-01-24 17:36:00.315689	2022-01-25 17:36:00.315689	rRpRW2AIYkGkS7lSlytbzMdYYh9cFqNch58qwv0EkAmKcOgnVKgdqd6AmesmkyaI0x6Pe6yQH6C6k245bcB47p2kG1Fc5ixd7xyXYnLpV0J38Bo9jZ5XL27Fcw8lqs6zyAQmRKuNsuW4o5RJmHyPrVIL6pFnAhtnKuLkoKhYLC3fi8S4nwIS1NSy1Ir2rZVvdutZVtV3HSS3FBEU84qbmhGixZErmWVHYCpSfsy4PrthaC5JuNpaHcJpr3VHQObh	f	1
\.


--
-- TOC entry 3082 (class 0 OID 32772)
-- Dependencies: 201
-- Data for Name: User; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."User" (id, nickname, pass_salt, email, avatar, created_at) FROM stdin;
1	morsisko	$2b$12$BRXrnIe/lhpTFnBTnRmJduIXzd4mUIuq9kmWhZvYZQZZBeQ0RZgNq	morsisko@gmail.com	avatar.png	2022-01-06 19:47:39.098694
2	kubsonhehe	$2b$12$8IXkWiHkMEw1uNGWA9wkIea7Niq4E7BFoH0EZyarmRAEn01jfsy5W	kubson@gmail.com	avatar.png	2022-01-08 14:35:57.291223
\.


--
-- TOC entry 3126 (class 0 OID 0)
-- Dependencies: 220
-- Name: AssignedRole_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."AssignedRole_id_seq"', 1, false);


--
-- TOC entry 3127 (class 0 OID 0)
-- Dependencies: 202
-- Name: CarBrand_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."CarBrand_id_seq"', 1, true);


--
-- TOC entry 3128 (class 0 OID 0)
-- Dependencies: 204
-- Name: CarModel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."CarModel_id_seq"', 1, true);


--
-- TOC entry 3129 (class 0 OID 0)
-- Dependencies: 206
-- Name: Car_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Car_id_seq"', 1, false);


--
-- TOC entry 3130 (class 0 OID 0)
-- Dependencies: 208
-- Name: Device_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Device_id_seq"', 1, false);


--
-- TOC entry 3131 (class 0 OID 0)
-- Dependencies: 218
-- Name: EmailConfirmation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."EmailConfirmation_id_seq"', 1, false);


--
-- TOC entry 3132 (class 0 OID 0)
-- Dependencies: 214
-- Name: Firmware_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Firmware_id_seq"', 1, false);


--
-- TOC entry 3133 (class 0 OID 0)
-- Dependencies: 216
-- Name: ForgottenPasswords_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."ForgottenPasswords_id_seq"', 1, false);


--
-- TOC entry 3134 (class 0 OID 0)
-- Dependencies: 212
-- Name: Leaderboard_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Leaderboard_id_seq"', 24, true);


--
-- TOC entry 3135 (class 0 OID 0)
-- Dependencies: 210
-- Name: Record_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Record_id_seq"', 11, true);


--
-- TOC entry 3136 (class 0 OID 0)
-- Dependencies: 222
-- Name: Role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Role_id_seq"', 1, false);


--
-- TOC entry 3137 (class 0 OID 0)
-- Dependencies: 224
-- Name: Session_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Session_id_seq"', 17, true);


--
-- TOC entry 3138 (class 0 OID 0)
-- Dependencies: 200
-- Name: User_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."User_id_seq"', 2, true);


--
-- TOC entry 2930 (class 2606 OID 32882)
-- Name: AssignedRole AssignedRole_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."AssignedRole"
    ADD CONSTRAINT "AssignedRole_pkey" PRIMARY KEY (id);


--
-- TOC entry 2912 (class 2606 OID 32792)
-- Name: CarBrand CarBrand_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."CarBrand"
    ADD CONSTRAINT "CarBrand_pkey" PRIMARY KEY (id);


--
-- TOC entry 2914 (class 2606 OID 32803)
-- Name: CarModel CarModel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."CarModel"
    ADD CONSTRAINT "CarModel_pkey" PRIMARY KEY (id);


--
-- TOC entry 2916 (class 2606 OID 32814)
-- Name: Car Car_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Car"
    ADD CONSTRAINT "Car_pkey" PRIMARY KEY (id);


--
-- TOC entry 2918 (class 2606 OID 32825)
-- Name: Device Device_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Device"
    ADD CONSTRAINT "Device_pkey" PRIMARY KEY (id);


--
-- TOC entry 2928 (class 2606 OID 32874)
-- Name: EmailConfirmation EmailConfirmation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."EmailConfirmation"
    ADD CONSTRAINT "EmailConfirmation_pkey" PRIMARY KEY (id);


--
-- TOC entry 2924 (class 2606 OID 32855)
-- Name: Firmware Firmware_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Firmware"
    ADD CONSTRAINT "Firmware_pkey" PRIMARY KEY (id);


--
-- TOC entry 2926 (class 2606 OID 32863)
-- Name: ForgottenPasswords ForgottenPasswords_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ForgottenPasswords"
    ADD CONSTRAINT "ForgottenPasswords_pkey" PRIMARY KEY (id);


--
-- TOC entry 2922 (class 2606 OID 32844)
-- Name: Leaderboard Leaderboard_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Leaderboard"
    ADD CONSTRAINT "Leaderboard_pkey" PRIMARY KEY (id);


--
-- TOC entry 2920 (class 2606 OID 32836)
-- Name: Record Record_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Record"
    ADD CONSTRAINT "Record_pkey" PRIMARY KEY (id);


--
-- TOC entry 2932 (class 2606 OID 32893)
-- Name: Role Role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Role"
    ADD CONSTRAINT "Role_pkey" PRIMARY KEY (id);


--
-- TOC entry 2934 (class 2606 OID 32907)
-- Name: Session Session_cookie_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Session"
    ADD CONSTRAINT "Session_cookie_key" UNIQUE (cookie);


--
-- TOC entry 2936 (class 2606 OID 32905)
-- Name: Session Session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Session"
    ADD CONSTRAINT "Session_pkey" PRIMARY KEY (id);


--
-- TOC entry 2910 (class 2606 OID 32781)
-- Name: User User_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."User"
    ADD CONSTRAINT "User_pkey" PRIMARY KEY (id);


--
-- TOC entry 2947 (class 2606 OID 32958)
-- Name: AssignedRole AssignedRole_role_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."AssignedRole"
    ADD CONSTRAINT "AssignedRole_role_fkey" FOREIGN KEY (role) REFERENCES public."Role"(id);


--
-- TOC entry 2946 (class 2606 OID 32953)
-- Name: AssignedRole AssignedRole_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."AssignedRole"
    ADD CONSTRAINT "AssignedRole_user_fkey" FOREIGN KEY ("user") REFERENCES public."User"(id);


--
-- TOC entry 2937 (class 2606 OID 32908)
-- Name: Car Car_brand_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Car"
    ADD CONSTRAINT "Car_brand_fkey" FOREIGN KEY (brand) REFERENCES public."CarBrand"(id);


--
-- TOC entry 2938 (class 2606 OID 32913)
-- Name: Car Car_model_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Car"
    ADD CONSTRAINT "Car_model_fkey" FOREIGN KEY (model) REFERENCES public."CarModel"(id);


--
-- TOC entry 2940 (class 2606 OID 32923)
-- Name: Device Device_firmware_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Device"
    ADD CONSTRAINT "Device_firmware_fkey" FOREIGN KEY (firmware) REFERENCES public."Firmware"(id);


--
-- TOC entry 2939 (class 2606 OID 32918)
-- Name: Device Device_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Device"
    ADD CONSTRAINT "Device_owner_fkey" FOREIGN KEY (owner) REFERENCES public."User"(id);


--
-- TOC entry 2945 (class 2606 OID 32948)
-- Name: EmailConfirmation EmailConfirmation_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."EmailConfirmation"
    ADD CONSTRAINT "EmailConfirmation_owner_fkey" FOREIGN KEY (owner) REFERENCES public."User"(id);


--
-- TOC entry 2944 (class 2606 OID 32943)
-- Name: ForgottenPasswords ForgottenPasswords_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ForgottenPasswords"
    ADD CONSTRAINT "ForgottenPasswords_owner_fkey" FOREIGN KEY (owner) REFERENCES public."User"(id);


--
-- TOC entry 2943 (class 2606 OID 32938)
-- Name: Leaderboard Leaderboard_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Leaderboard"
    ADD CONSTRAINT "Leaderboard_user_fkey" FOREIGN KEY ("user") REFERENCES public."User"(id);


--
-- TOC entry 2942 (class 2606 OID 32933)
-- Name: Record Record_car_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Record"
    ADD CONSTRAINT "Record_car_fkey" FOREIGN KEY (car) REFERENCES public."Car"(id);


--
-- TOC entry 2941 (class 2606 OID 32928)
-- Name: Record Record_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Record"
    ADD CONSTRAINT "Record_owner_fkey" FOREIGN KEY (owner) REFERENCES public."User"(id);


--
-- TOC entry 2948 (class 2606 OID 32963)
-- Name: Session Session_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Session"
    ADD CONSTRAINT "Session_owner_fkey" FOREIGN KEY (owner) REFERENCES public."User"(id);


--
-- TOC entry 3112 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2022-01-24 18:08:25 UTC

--
-- PostgreSQL database dump complete
--

